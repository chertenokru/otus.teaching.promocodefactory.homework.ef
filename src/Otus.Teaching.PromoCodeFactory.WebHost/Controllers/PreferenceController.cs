﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private readonly IRepository<Preference> preference;
        private readonly ILogger logger;

        public PreferenceController(IRepository<Preference> preference, ILogger<PreferenceController> logger)
        {
            this.preference = preference;
            this.logger = logger;
        }

        /// <summary>
        /// Список предпочтений по именам
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PreferenceResponse>>> GetAll()
        {
            try
            {
                var preferences = await preference.GetAllAsync();
                return Ok(preferences.Select(x => PreferenceResponse.GetPreferenceResponse(x)).ToList());
            }
            catch (Exception e)
            {
                logger.LogError(e.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Список предпочтений по id,name
        /// </summary>
        /// <returns></returns>
        [HttpGet("full")]
        public async Task<ActionResult<IEnumerable<PreferenceResponseFull>>> GetIdAll()
        {
            try
            {
                var preferences = await preference.GetAllAsync();
                return Ok(preferences.Select(x => PreferenceResponseFull.GetPreferenceResponse(x)).ToList());
            }
            catch (Exception e)
            {
                logger.LogError(e.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}