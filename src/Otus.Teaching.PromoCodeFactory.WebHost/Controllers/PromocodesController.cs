﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> promoCodes;
        private readonly IRepository<Preference> preferences;
        private readonly IRepository<Employee> employee;
        private readonly ILogger logger;

        public PromocodesController(IRepository<PromoCode> promoCodes,IRepository<Employee> employee,
            IRepository<Preference> preferences,ILogger<PromocodesController> logger)
        {
            this.promoCodes = promoCodes;
            this.preferences = preferences;
            this.employee = employee;
            this.logger = logger;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            try
            {
                var codes = await promoCodes.GetAllAsync();
                return Ok(codes.Select(PromoCodeShortResponse.GetPromoCodeShortResponse).ToList());
            }catch (Exception e)
            {
                logger.LogError(e.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns>возвращает список Id клиентов и Id выданных кодов</returns>
        [HttpPost]
        public async Task<ActionResult<IEnumerable<CustomerAndPromoCode>>> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var resultList = new List<CustomerAndPromoCode>();
            try
            {
                
                var prefs = (await preferences.GetAsync(
                    x => x.Name == request.Preference,
                    nameof(Preference.CustomerPreferenses))).FirstOrDefault();

                if (prefs is null)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, $"Предпочтение {request.Preference} не найдено");
                }

                if (prefs.CustomerPreferenses is null || prefs.CustomerPreferenses.Count == 0)
                {
                    return StatusCode(StatusCodes.Status404NotFound,
                        $"Предпочтение {request.Preference} не найдено у клиентов");
                }

                foreach (var customerPreferense in prefs.CustomerPreferenses)
                {
                    var promoCode = GivePromoCodeRequest.GetPromoCode(request);
                    promoCode.CustomerID = customerPreferense.CustomerID;
                    promoCode.PreferenceID = customerPreferense.PreferenceID;
                    promoCode.PartnerManager = (await employee.GetAsync(x=>x.Role.Name == "PartnerManager")).FirstOrDefault();
                    if (promoCode.PartnerManager is null) return BadRequest("Нет PartnerManager-а");
                    await promoCodes.CreateAsync(promoCode);
                    resultList.Add(CustomerAndPromoCode.GetCustomerAndPromoCode(promoCode));
                }
                await promoCodes.SaveChanges();
                return Ok(resultList);
            }
            catch (Exception e)
            {
                logger.LogError(e.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}