﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerRepository customers;
        private readonly ILogger logger;

        public CustomersController(ICustomerRepository customers, ILogger<CustomersController> logger)
        {
            this.customers = customers;
            this.logger = logger;
        }

        /// <summary>
        /// Возвращает список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            try
            {
                return Ok(CustomerShortResponse.GetList(await customers.GetAllAsync()));
            }
            catch (Exception e)
            {
                logger.LogError(e.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Возавращает клиента по id
        /// </summary>
        /// <param name="id">id клиента</param>
        /// <returns>Расширенная информация по клиенту с предпочтениями и кодами</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            try
            {
                return Ok(CustomerResponse.GetCustomerResponse(await customers.GetByIdAsync(id)));
            }
            catch (Exception e)
            {
                logger.LogError(e.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        ///Создаёт нового клента со списком предпочтений
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            try
            {
                var newCustomer = CreateOrEditCustomerRequest.GetCustomer(request);
                await customers.CreateAsync(newCustomer);
                await customers.SaveChanges();
            }
            catch (Exception e)
            {
                logger.LogError(e.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }

        /// <summary>
        /// Обновляет клиента и его предпочтения по id 
        /// </summary>
        /// <param name="id">id клиента</param>
        /// <param name="request">данные клиента</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            try
            {
                var updCustomer = await customers.GetByIdAsync(id);
                if (updCustomer == null) return NotFound();

                updCustomer = CreateOrEditCustomerRequest.GetCustomer(request, updCustomer);
                await customers.UpdateAsync(updCustomer);
                await customers.SaveChanges();
            }
            catch (KeyNotFoundException e)
            {
                return NotFound();
            }
            catch (ArgumentNullException e)
            {
                return BadRequest();
            }
            catch (Exception e)
            {
                logger.LogError(e.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }

        /// <summary>
        /// Удаляет клиента вместе с  кодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            try
            {
                await customers.DeleteByIdAsync(id);
                await customers.SaveChanges();
            }
            catch (KeyNotFoundException e)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                logger.LogError(e.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }
    }
}