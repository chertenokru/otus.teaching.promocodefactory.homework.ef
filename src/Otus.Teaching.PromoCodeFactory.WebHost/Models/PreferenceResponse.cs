﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public string Name { get; set; }

        public static PreferenceResponse GetPreferenceResponse(Preference x)
        {
            return new PreferenceResponse() {Name = x.Name};
        }
    }
}