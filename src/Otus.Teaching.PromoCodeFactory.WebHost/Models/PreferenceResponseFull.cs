﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponseFull
    {
        public string Id  { get; set; }
        public string Name  { get; set; }

        public static PreferenceResponseFull GetPreferenceResponse(Preference preference)
        {
            return new PreferenceResponseFull() {Id = preference.Id.ToString(), Name = preference.Name};
        }
    }
}