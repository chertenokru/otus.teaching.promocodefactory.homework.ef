﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerShortResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public static IEnumerable<CustomerShortResponse> GetList(IEnumerable<Customer> customers)
        {
            return customers.Select(x => new CustomerShortResponse()
            {
                Email = x.Email,
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
        }

    }
}