﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceResponse> Preferences { get; set; }
        public List<PromoCodeShortResponse> PromoCodes { get; set; }

        public static CustomerResponse GetCustomerResponse(Customer customer)
        {
            return new CustomerResponse()
            {
                Id = customer.Id, Email = customer.Email, Preferences = customer.CustomerPreferences.Select(x =>
                    PreferenceResponse.GetPreferenceResponse(x.Preference)).ToList(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes.Select(x =>
                    PromoCodeShortResponse.GetPromoCodeShortResponse(x)).ToList()
            };
        }

    }
}