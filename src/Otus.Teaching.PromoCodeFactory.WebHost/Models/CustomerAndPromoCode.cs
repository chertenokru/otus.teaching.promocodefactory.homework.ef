﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerAndPromoCode
    {
        public Guid CustomerId { get; set; }
        public Guid PromoCodeId { get; set; }

        public static CustomerAndPromoCode GetCustomerAndPromoCode(PromoCode promoCode)
        {
            return new CustomerAndPromoCode()
            {
                CustomerId = promoCode.CustomerID,
                PromoCodeId = promoCode.Id
            };
        }
    }
}