﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        public string ServiceInfo { get; set; }

        public string PartnerName { get; set; }

        public string PromoCode { get; set; }

        public string Preference { get; set; }

        public static PromoCode GetPromoCode(GivePromoCodeRequest request)
        {
            return new PromoCode()
            {
                Id = new Guid() ,
                Code = request.PromoCode,
                BeginDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(30),
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo
            };
        }
        
    }
}