﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateOrEditCustomerRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Guid> PreferenceIds { get; set; }

        public static Customer GetCustomer(CreateOrEditCustomerRequest request, Customer oldCustomer = null)
        {
            if (oldCustomer == null)
                oldCustomer = new Customer()
                {
                    Id = Guid.NewGuid()
                };
            oldCustomer.Email = request.Email;
            oldCustomer.FirstName = request.FirstName;
            oldCustomer.LastName = request.LastName;
            oldCustomer.CustomerPreferences?.Clear();

            oldCustomer.CustomerPreferences =
                request.PreferenceIds.Select(x => new CustomerPreferences()
                    {
                        CustomerID = oldCustomer.Id,
                        PreferenceID = x
                    }
                ).ToList();
            return oldCustomer;
        }
    }
}