﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync(params string[] includedPropertyNames);

        Task<T> GetByIdAsync(Guid id, params string[] includedPropertyNames);

        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> predicate,
            params string[] includedPropertyNames);

        IQueryable<T>GetQueryable(Expression<Func<T, bool>> predicate,
            params string[] includedPropertyNames);

        Task DeleteByIdAsync(Guid id);

        Task CreateAsync(T entity);

        Task UpdateAsync(T entity);

        Task SaveChanges();
    }
}