﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreferences
    {
        public Guid CustomerID { get; set; }
        public Customer Customer { get; set; }
        public Guid PreferenceID { get; set; }
        public Preference Preference { get; set; }
    }
}