﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<CustomerPreferences> CustomerPreferenses { get; set; }
        public ICollection<PromoCode> PromoCodes { get; set; }
    }
}