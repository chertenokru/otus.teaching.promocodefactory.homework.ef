﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfCustomerRepository : EfRepository<Customer>,ICustomerRepository
    {
        public EfCustomerRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {
            return await dbContext.Set<Customer>().Where(x => x.Id == id)
                .Include(x => x.CustomerPreferences).ThenInclude(y => y.Preference)
                .Include(x => x.PromoCodes).FirstOrDefaultAsync();
        }

       
    }
}