﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected AppDbContext dbContext;

        public EfRepository(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(params string[] includedPropertyNames)
        {
            return await IncludeProperties(dbContext.Set<T>(), includedPropertyNames).ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(Guid id, params string[] includedPropertyNames)
        {
            return await IncludeProperties(dbContext.Set<T>(), includedPropertyNames)
                .Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public virtual async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> predicate,
            params string[] includedPropertyNames)
        {
            return await IncludeProperties(dbContext.Set<T>(), includedPropertyNames).Where(predicate).ToListAsync();
        }

        public virtual  IQueryable<T> GetQueryable(Expression<Func<T, bool>> predicate, params string[] includedPropertyNames)
        {
            return  IncludeProperties(dbContext.Set<T>(), includedPropertyNames).Where(predicate);
        }

        public virtual async Task DeleteByIdAsync(Guid id)
        {
            var entity = await dbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            if (entity != null) dbContext.Set<T>().Remove(entity);
            else throw new KeyNotFoundException();
        }

        public async Task CreateAsync(T entity)
        {
            await dbContext.Set<T>().AddAsync(entity);
        }

        public  Task UpdateAsync(T entity)
        {
            if (entity is null) throw new ArgumentNullException("Entity is null");
            
                 dbContext.Set<T>().Update(entity);
                 return Task.CompletedTask;
        }


        public async Task SaveChanges()
        {
            await dbContext.SaveChangesAsync();
        }

        private IQueryable<T> IncludeProperties(IQueryable<T> query, params string[] propertyNames)
        {
            if (propertyNames == null || propertyNames.Length < 1)
                return query;

            foreach (var name in propertyNames)
                query = query.Include(name);

            return query;
        }
    }
}