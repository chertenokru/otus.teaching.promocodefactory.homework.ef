﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class AppDbContext : DbContext
    {
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerPreferences> CustomersPreferenses { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasKey(x => x.Id);
            modelBuilder.Entity<Role>().Property(x => x.Name).HasMaxLength(50).IsRequired();
            modelBuilder.Entity<Role>().Property(x => x.Description).HasMaxLength(250).IsRequired();

            modelBuilder.Entity<Employee>().HasKey(x => x.Id);
            modelBuilder.Entity<Employee>().Property(x => x.Email).HasMaxLength(150).IsRequired();
            modelBuilder.Entity<Employee>().Property(x => x.FirstName).HasMaxLength(250).IsRequired();
            modelBuilder.Entity<Employee>().Property(x => x.LastName).HasMaxLength(250).IsRequired();
            modelBuilder.Entity<Employee>().Property(x => x.AppliedPromocodesCount).HasDefaultValue(0).IsRequired();
            modelBuilder.Entity<Employee>().HasOne(x => x.Role).WithMany()
                .HasForeignKey("RoleId").IsRequired();

            modelBuilder.Entity<Customer>().HasKey(x => x.Id);
            modelBuilder.Entity<Customer>().Property(x => x.Email).HasMaxLength(150).IsRequired();
            modelBuilder.Entity<Customer>().Property(x => x.FirstName).HasMaxLength(250).IsRequired();
            modelBuilder.Entity<Customer>().Property(x => x.LastName).HasMaxLength(250).IsRequired();
            modelBuilder.Entity<Customer>().HasMany(x => x.PromoCodes).WithOne(x=>x.Customer).OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<CustomerPreferences>().HasKey(x => new {PreferenseID = x.PreferenceID, x.CustomerID});
            modelBuilder.Entity<CustomerPreferences>().HasOne(x => x.Customer)
                .WithMany(x => x.CustomerPreferences).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<CustomerPreferences>().HasOne(x => x.Preference)
                .WithMany(x => x.CustomerPreferenses);

            modelBuilder.Entity<Preference>().HasKey(x => x.Id);
            modelBuilder.Entity<Preference>().Property(x => x.Name).HasMaxLength(150).IsRequired();
            modelBuilder.Entity<Preference>().Property(x => x.Description).HasMaxLength(250);

            modelBuilder.Entity<PromoCode>().HasKey(x => x.Id);
            modelBuilder.Entity<PromoCode>().Property(x => x.Code).HasMaxLength(250).IsRequired();
            modelBuilder.Entity<PromoCode>().Property(x => x.BeginDate).IsRequired();
            modelBuilder.Entity<PromoCode>().Property(x => x.EndDate).IsRequired();
            modelBuilder.Entity<PromoCode>().Property(x => x.PartnerName).HasMaxLength(250).IsRequired();
            modelBuilder.Entity<PromoCode>().Property(x => x.ServiceInfo).HasMaxLength(250);
            modelBuilder.Entity<PromoCode>().HasOne(x => x.Preference).WithMany(x=>x.PromoCodes).IsRequired();
            modelBuilder.Entity<PromoCode>().HasOne(x => x.PartnerManager).WithMany(x=>x.PromoCodes).IsRequired();


            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<CustomerPreferences>().HasData(FakeDataFactory.CustomersPreferenses);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<PromoCode>().HasData(FakeDataFactory.PromoCodes);

            base.OnModelCreating(modelBuilder);
        }
    }
}