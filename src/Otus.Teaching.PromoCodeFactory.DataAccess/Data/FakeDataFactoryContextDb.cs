﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static  class FakeDataFactoryContextDb
    {
       
        
         public static  void  EmployeesInit(AppDbContext dbContext)  
        {
            foreach (var employee in FakeDataFactory.Employees)
            {
                if (!dbContext.Roles.Any(x => x.Id == employee.Id))
                {
                    if (dbContext.Roles.Any(x => x.Id == employee.Role.Id))
                    {
                        employee.Role = dbContext.Roles.FirstOrDefault(x => x.Id == employee.Role.Id);
                    }

                    dbContext.Employees.Add(employee);
                }
            }
        }

         public static void RolesInit(AppDbContext dbContext)
         {
             foreach (var role in FakeDataFactory.Roles)
             {
                 if (!dbContext.Roles.Any(x => x.Id == role.Id))
                 {
                     dbContext.Roles.Add(role);
                 }
             }
         }
        
         
        public static void PreferencesInit(AppDbContext dbContext)
        {
            foreach (var preference in FakeDataFactory.Preferences)
            {
                if (!dbContext.Preferences.Any(x => x.Id == preference.Id))
                {
                    dbContext.Preferences.Add(preference);
                }
            }   
        }

     
    }
}