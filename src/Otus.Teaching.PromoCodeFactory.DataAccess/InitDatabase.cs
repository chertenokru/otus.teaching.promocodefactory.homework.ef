﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class InitDatabase
    {
        private AppDbContext dbContext;

        public InitDatabase(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Init(bool deleteOldBD)
        {
            // удаляем бд и накатываем миграции
            if (deleteOldBD)
            {
                dbContext.Database.EnsureDeleted();

                if (dbContext.Database.GetMigrations().Any()) dbContext.Database.Migrate();
                else dbContext.Database.EnsureCreated();
            }

            // Первоначальный вариант вставки данных, не стал доделывать,
        // отказался в пользу HasDATA
        //    FakeDataFactoryContextDb.RolesInit(dbContext);
        //    dbContext.SaveChanges();
        //    FakeDataFactoryContextDb.EmployeesInit(dbContext);
        //    dbContext.SaveChanges();
            
        }
    }
}